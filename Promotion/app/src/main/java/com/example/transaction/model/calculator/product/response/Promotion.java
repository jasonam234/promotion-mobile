package com.example.transaction.model.calculator.product.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Promotion {
    @SerializedName("promotion_id")
    @Expose
    private Integer promotionId;
    @SerializedName("discount_value")
    @Expose
    private Integer discountValue;
    @SerializedName("promo_value")
    @Expose
    private String promoValue;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("product_code")
    @Expose
    private String productCode;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("promotion_free_goods_id")
    @Expose
    private Integer promotionFreeGoodsId;
    @SerializedName("price")
    @Expose
    private Integer price;

    public Integer getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Integer promotionId) {
        this.promotionId = promotionId;
    }

    public Integer getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(Integer discountValue) {
        this.discountValue = discountValue;
    }

    public String getPromoValue() {
        return promoValue;
    }

    public void setPromoValue(String promoValue) {
        this.promoValue = promoValue;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getPromotionFreeGoodsId() {
        return promotionFreeGoodsId;
    }

    public void setPromotionFreeGoodsId(Integer promotionFreeGoodsId) {
        this.promotionFreeGoodsId = promotionFreeGoodsId;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
