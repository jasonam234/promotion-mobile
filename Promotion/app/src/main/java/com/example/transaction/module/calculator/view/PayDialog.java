package com.example.transaction.module.calculator.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.transaction.R;

public class PayDialog extends AppCompatDialogFragment {
    private EditText editTextDescription;
    private Button payButton;
    private PayDialog.PayListener listener;

    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_post_item_dialog, null);

        payButton = view.findViewById(R.id.payButton);
        editTextDescription = view.findViewById(R.id.editTextDescription);

        builder.setView(view)
                .setTitle("Pay")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        payButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.postTransaction(editTextDescription.getText().toString());
                dismiss();
            }
        });

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (PayDialog.PayListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement PayListener");
        }
    }

    public interface PayListener{
        void postTransaction(String description);
    }
}
