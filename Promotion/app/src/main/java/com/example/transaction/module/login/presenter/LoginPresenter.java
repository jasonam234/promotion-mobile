package com.example.transaction.module.login.presenter;

import android.content.Context;
import android.util.Log;

import com.example.transaction.model.login.LoginResponse;
import com.example.transaction.model.login.User;
import com.example.transaction.module.login.LoginContract;
import com.example.transaction.generator.ServiceGenerator;
import com.example.transaction.module.login.services.LoginServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter implements LoginContract.Presenter {
    private final Context context;
    private LoginServices service;
    LoginContract.View loginView;


    public LoginPresenter(Context context, LoginContract.View loginView){
        this.loginView = loginView;
        this.context = context;
    }

    @Override
    public void initPresenter(){
        loginView.initView();
        service = ServiceGenerator.createService(LoginServices.class);
    }

    @Override
    public void loginClickedPresenter(String username, String password){
        User user = new User(username, password);
        Call<LoginResponse> response = service.authUser(user);
        response.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.code() == 200){
                    loginView.setToken(response.body().getData());
                }else if(response.code() == 500){
                    loginView.loginStatus("Wrong username or password");
                    loginView.setLoginButton(true);
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.d("fail", Log.getStackTraceString(t));
                loginView.loginStatus("Connection Error.");
                loginView.setLoginButton(true);
            }
        });
    }
}
