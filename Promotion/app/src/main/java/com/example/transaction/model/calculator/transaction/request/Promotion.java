package com.example.transaction.model.calculator.transaction.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Promotion {
    @SerializedName("promotion_id")
    @Expose
    private Integer promotionId;

    public Integer getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Integer promotionId) {
        this.promotionId = promotionId;
    }
}
