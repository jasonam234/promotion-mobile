package com.example.transaction.model.calculator.transaction.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostTransaction {
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("promotion")
    @Expose
    private Promotion promotion;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("total_price")
    @Expose
    private Integer totalPrice;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }
}
