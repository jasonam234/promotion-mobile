package com.example.transaction.module.calculator.presenter;

import android.content.Context;
import android.util.Log;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.example.transaction.R;
import com.example.transaction.generator.ServiceGenerator;
import com.example.transaction.model.calculator.detailTransaction.request.DetailTransaction;
import com.example.transaction.model.calculator.detailTransaction.request.Product;
import com.example.transaction.model.calculator.detailTransaction.request.Transaction;
import com.example.transaction.model.calculator.detailTransaction.response.TransactionDetailResponse;
import com.example.transaction.model.calculator.product.request.ProductRecycler;
import com.example.transaction.model.calculator.product.response.ProductResponse;
import com.example.transaction.model.calculator.product.response.PromoResponse;
import com.example.transaction.model.calculator.transaction.request.PostTransaction;
import com.example.transaction.model.calculator.transaction.request.Promotion;
import com.example.transaction.model.calculator.transaction.response.TransactionResponse;
import com.example.transaction.model.calculator.transaction.request.User;
import com.example.transaction.module.calculator.CalculatorContract;
import com.example.transaction.module.calculator.services.CalculatorServices;
import com.example.transaction.module.calculator.view.AddItemDialog;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CalculatorPresenter implements CalculatorContract.Presenter {
    private final Context context;
    private CalculatorServices service;
    CalculatorContract.View calculatorView;
    CalculatorContract.Dialog calculatorDialog;
    CalculatorContract.InformationDialog informationDialog;
    Transaction transactionId = new Transaction();

    public CalculatorPresenter(Context context, CalculatorContract.View calculatorView){
        this.calculatorView = calculatorView;
        this.context = context;
    }

    public CalculatorPresenter(Context context, CalculatorContract.Dialog calculatorDialog){
        this.calculatorDialog = calculatorDialog;
        this.context = context;
    }

    public CalculatorPresenter(Context context
            , CalculatorContract.InformationDialog informationDialog){
        this.informationDialog = informationDialog;
        this.context = context;
    }

    @Override
    public void initPresenter(){
        calculatorView.initView();
        service = ServiceGenerator.createService(CalculatorServices.class);
    }

    @Override
    public void initPresenterDialog(){
        service = ServiceGenerator.createService(CalculatorServices.class);
    }

    @Override
    public void postTransaction(int userId, String description, int totalPrice
            , ArrayList<ProductRecycler> product, int promotionId, String bearerToken){
        User user = new User();
        user.setUserId(userId);
        Promotion promotion = new Promotion();
        promotion.setPromotionId(promotionId);
        PostTransaction transaction = new PostTransaction();
        transaction.setDescription(description);
        transaction.setTotalPrice(totalPrice);
        transaction.setUser(user);
        transaction.setPromotion(promotion);

        Call<TransactionResponse> response = service.postTransaction(bearerToken, transaction);
        response.enqueue(new Callback<TransactionResponse>() {
            @Override
            public void onResponse(Call<TransactionResponse> call
                    , Response<TransactionResponse> response) {
                transactionId.setTransactionId(response.body().getData().getTransactionId());
                postDetailTransaction(product, userId, transactionId, bearerToken);
            }

            @Override
            public void onFailure(Call<TransactionResponse> call, Throwable t) {
                calculatorView.progressBarStatus(false);
                calculatorView.sweetAlert("fail", "Connection Error");
            }
        });
    }

    @Override
    public void postDetailTransaction(ArrayList<ProductRecycler> product, int userId
            , Transaction transactionId, String bearerToken){
        Product productData = new Product();
        DetailTransaction detailTransaction = new DetailTransaction();
        for(int i = 0; i < product.size(); i++){
            productData.setProductId(product.get(i).getProductId());
            detailTransaction.setProduct(productData);
            detailTransaction.setTransaction(transactionId);
            detailTransaction.setPrice(product.get(i).getPrice());
            detailTransaction.setQuantity(product.get(i).getQuantity());

            Call<TransactionDetailResponse> response = service.postTransactionDetail(bearerToken
                    , detailTransaction);
            response.enqueue(new Callback<TransactionDetailResponse>() {
                @Override
                public void onResponse(Call<TransactionDetailResponse> call
                        , Response<TransactionDetailResponse> response) {
                    if(response.body().getStatus() == 200){
                        calculatorView.clearData();
                        calculatorView.progressBarStatus(false);
                        calculatorView.sweetAlert("success", "Transaction Posted");
                    }else{
                        calculatorView.sweetAlert("failure", "Transaction Not Posted");
                    }
                }

                @Override
                public void onFailure(Call<TransactionDetailResponse> call, Throwable t) {
                    calculatorView.progressBarStatus(false);
                    calculatorView.sweetAlert("fail", "Connection Error");
                }
            });
        }
    }

    @Override
    public void searchItem(String bearerToken, String productCode){
        service = ServiceGenerator.createService(CalculatorServices.class);
        Call<ProductResponse> response = service.getProduct(bearerToken, productCode);
        response.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if(response.code() == 200){
                    calculatorDialog.setLoading(false);
                    calculatorDialog.updateItemDialog(response.body().getData().getName()
                            , response.body().getData().getPrice()
                            , response.body().getData().getProductId());
                }else{
                    calculatorDialog.setLoading(false);
                    calculatorDialog.setWarning("Product Not Found", "product");
                }

            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                calculatorDialog.setLoading(false);
                calculatorDialog.setWarning("Connection Error", "message");
            }
        });
    }

    @Override
    public void quantityValidation(String bearerToken, String productCode, int quantity){
        Call<ProductResponse> response = service
                .getProductAndQuantity(bearerToken, productCode, quantity);
        response.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call
                    , Response<ProductResponse> response) {
                if(response.code() == 200){
                    calculatorDialog.setLoading(false);
                    calculatorDialog.postEntry();
                }else{
                    calculatorDialog.setLoading(false);
                    calculatorDialog.setWarning("Insufficient Quantity", "message");
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                calculatorDialog.setWarning("Connection Error", "message");
                calculatorDialog.setLoading(false);
            }
        });
    }

    @Override
    public void applyDiscount(String bearerToken, ArrayList<ProductRecycler> product){
        Call<PromoResponse> responseFreeGoods = service.searchFreeGoodsPromo(bearerToken, product);
        responseFreeGoods.enqueue(new Callback<PromoResponse>() {
            @Override
            public void onResponse(Call<PromoResponse> call, Response<PromoResponse> response) {
                if(response.code() == 200){
                    calculatorView.applyFreeGoods(response.body().getData().getName()
                            , response.body().getData().getQuantity()
                            , response.body().getData().getProductId()
                            , response.body().getData().getPromotionId());
                }else if(response.code() == 500){
                    Call<PromoResponse> responseDiscount = service.searchDiscountPromo(bearerToken
                            , product);
                    responseDiscount.enqueue(new Callback<PromoResponse>() {
                        @Override
                        public void onResponse(Call<PromoResponse> call
                                , Response<PromoResponse> response) {
                            if(response.body().getStatus() == 200){
                                calculatorView.applyDiscount(response.body().getData()
                                                .getDiscountValue()
                                        , response.body().getData().getPrice()
                                        , response.body().getData().getProductId()
                                        , response.body().getData().getPromotionId()
                                        , response.body().getData().getName());
                            }else{
                                calculatorView.progressBarStatus(false);
                                calculatorView.sweetAlert("fail", "Promo Not Found");
                            }
                        }

                        @Override
                        public void onFailure(Call<PromoResponse> calls, Throwable t) {
                            calculatorView.progressBarStatus(false);
                            calculatorView.sweetAlert("fail", "Connection Error");
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<PromoResponse> call, Throwable t) {
                calculatorView.progressBarStatus(false);
                calculatorView.sweetAlert("fail", "Connection Error");
            }
        });
    }
}
