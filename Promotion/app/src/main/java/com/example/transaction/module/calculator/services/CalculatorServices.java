package com.example.transaction.module.calculator.services;

import com.example.transaction.model.calculator.detailTransaction.request.DetailTransaction;
import com.example.transaction.model.calculator.detailTransaction.response.TransactionDetailResponse;
import com.example.transaction.model.calculator.product.request.ProductRecycler;
import com.example.transaction.model.calculator.product.response.PromoResponse;
import com.example.transaction.model.calculator.transaction.request.PostTransaction;
import com.example.transaction.model.calculator.product.response.ProductResponse;
import com.example.transaction.model.calculator.transaction.response.TransactionResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface CalculatorServices {
    @GET("api/product/{product_code}")
    Call<ProductResponse> getProduct(@Header("Authorization") String token,
                                     @Path("product_code") String productCode);

    @GET("api/product/{product_code}/{quantity}")
    Call<ProductResponse> getProductAndQuantity(@Header("Authorization") String token,
                                     @Path("product_code") String productCode,
                                     @Path("quantity") int quantity);

    @POST("api/transaction/post/transaction")
    Call<TransactionResponse> postTransaction(@Header("Authorization") String token
            , @Body PostTransaction transaction);

    @POST("api/transaction/post/transaction_detail")
    Call<TransactionDetailResponse> postTransactionDetail(@Header("Authorization") String token
            , @Body DetailTransaction detailTransaction);

    @POST("api/promotion/post/applied_promos/free_goods")
    Call<PromoResponse> searchFreeGoodsPromo(@Header("Authorization") String token
            , @Body ArrayList<ProductRecycler> productRecyclers);

    @POST("api/promotion/post/applied_promos/discount")
    Call<PromoResponse> searchDiscountPromo(@Header("Authorization") String token
            , @Body ArrayList<ProductRecycler> productRecyclers);
}
