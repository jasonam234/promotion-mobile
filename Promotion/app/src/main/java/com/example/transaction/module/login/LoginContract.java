package com.example.transaction.module.login;

public interface LoginContract {
    interface View {
        void initView();
        void loginStatus(String message);
        void setToken(String token);
        void setLoginButton(boolean status);
    }

    interface Presenter {
        void initPresenter();
        void loginClickedPresenter(String username, String password);
    }
}
