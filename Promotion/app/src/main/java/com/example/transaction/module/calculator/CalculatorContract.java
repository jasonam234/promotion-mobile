package com.example.transaction.module.calculator;

import com.example.transaction.model.calculator.detailTransaction.request.Transaction;
import com.example.transaction.model.calculator.product.request.ProductRecycler;

import java.util.ArrayList;

import retrofit2.Response;

public interface CalculatorContract {
    interface View {
        void initView();
        void openDialog();
        void clearData();
        void applyDiscount(int discountValue, int price, int productId, int promotionId
                , String itemName);
        void applyFreeGoods(String name, int quantity, int productId, int promotionId);
        void sweetAlert(String type, String title);
        void progressBarStatus(boolean status);
    }

    interface Presenter {
        void initPresenter();
        void initPresenterDialog();
        void postTransaction(int userId, String description, int totalPrice
                , ArrayList<ProductRecycler> product, int promotionId, String bearerToken);
        void postDetailTransaction(ArrayList<ProductRecycler> product, int userId
                , Transaction transactionId, String bearerToken);
        void searchItem(String bearerToken, String productCode);
        void applyDiscount(String bearerToken, ArrayList<ProductRecycler> product);
        void quantityValidation(String bearerToken, String productCode, int quantity);
    }

    interface Dialog {
        void updateItemDialog(String productName, int productPrice, int productId);
        void postEntry();
        void setWarning(String message, String type);
        void setLoading(boolean status);
    }

    interface InformationDialog {
    }
}
