package com.example.transaction.module.login.services;

import com.example.transaction.model.login.LoginResponse;
import com.example.transaction.model.login.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LoginServices {
    @POST("api/authenticate")
    Call<LoginResponse> authUser(@Body User user);
}
