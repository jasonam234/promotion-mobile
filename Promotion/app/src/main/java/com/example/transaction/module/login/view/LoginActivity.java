package com.example.transaction.module.login.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.transaction.R;
import com.example.transaction.module.calculator.view.CalculatorActivity;
import com.example.transaction.module.login.LoginContract;
import com.example.transaction.module.login.presenter.LoginPresenter;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {
    LoginContract.Presenter loginPresenter;
    EditText username;
    EditText password;
    Button buttonLogin;
    ProgressBar loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginPresenter = new LoginPresenter(this, this);
        loginPresenter.initPresenter();
    }

    public void initView(){
        username = (EditText) findViewById(R.id.editTextUsername);
        password = (EditText) findViewById(R.id.editTextPassword);
        loading = (ProgressBar) findViewById(R.id.progressBar);
        buttonLogin = (Button) findViewById(R.id.buttonLogin);

        loading.setVisibility(View.GONE);
        buttonLogin.setEnabled(true);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLoginButton(false);
                loginPresenter.loginClickedPresenter(username.getText().toString(),
                        password.getText().toString());
                loading.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void loginStatus(String message) {
        loading.setVisibility(View.GONE);
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast.makeText(context, message, duration).show();
    }

    @Override
    public void setToken(String token) {
        loading.setVisibility(View.GONE);
        SharedPreferences mToken = this
                .getSharedPreferences("token", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefEdit = mToken.edit();
        prefEdit.putString("token", token);
        prefEdit.commit();
        startActivity(new Intent(LoginActivity.this, CalculatorActivity.class));
        onStop();
    }

    @Override
    public void setLoginButton(boolean status){
        if(status){
            buttonLogin.setEnabled(true);
        }else{
            buttonLogin.setEnabled(false);
        }
    }

    @Override
    public void onStop () {
        super.onStop();
        finish();
    }

}