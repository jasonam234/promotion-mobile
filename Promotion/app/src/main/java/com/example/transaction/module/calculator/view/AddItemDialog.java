package com.example.transaction.module.calculator.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.transaction.R;
import com.example.transaction.generator.ServiceGenerator;
import com.example.transaction.model.calculator.product.response.ProductResponse;
import com.example.transaction.module.calculator.CalculatorContract;
import com.example.transaction.module.calculator.presenter.CalculatorPresenter;
import com.example.transaction.module.calculator.services.CalculatorServices;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.function.BinaryOperator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddItemDialog extends AppCompatDialogFragment implements CalculatorContract.Dialog{
    DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
    DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

    CalculatorContract.View calculatorView;

    private EditText editTextProductCode;
    private EditText editTextQuantity;
    private TextView editTextProductName;
    private TextView editTextProductPrice;
    private TextView warningText;
    private ImageButton searchProduct;
    private Button postItem;
    private CalculatorServices service;
    private DialogListener listener;
    private ProgressBar loading;
    int productPrice = 0;
    int quantity = 0;
    int productId = 0;
    boolean searched = false;
    CalculatorContract.Presenter calculatorPresenter;

    public AddItemDialog(CalculatorContract.Presenter calculatorPresenter) {
        this.calculatorPresenter = calculatorPresenter;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_add_item_dialog, null);

        SharedPreferences preferences = this.getActivity()
                .getSharedPreferences("token", Context.MODE_PRIVATE);
        String token = preferences.getString("token", "");
        String bearerToken = "Bearer " + token;

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        kursIndonesia.setDecimalFormatSymbols(formatRp);

        calculatorPresenter = new CalculatorPresenter(getContext(), this);
        calculatorPresenter.initPresenterDialog();

        editTextProductCode = view.findViewById(R.id.editTextProductCode);
        editTextProductName = view.findViewById(R.id.productNameValue);
        editTextProductPrice = view.findViewById(R.id.priceValue);
        editTextQuantity = view.findViewById(R.id.quantityValue);
        searchProduct = view.findViewById(R.id.searchProductButton);
        warningText = view.findViewById(R.id.warning);
        postItem = view.findViewById(R.id.postProductButton);
        loading = view.findViewById(R.id.progressBarAddItem);
        loading.setVisibility(View.GONE);

        service = ServiceGenerator.createService(CalculatorServices.class);

        builder.setView(view)
                .setTitle("Add New Item")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        searchProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLoading(true);
                calculatorPresenter.searchItem(bearerToken
                        , editTextProductCode.getText().toString());
            }
        });

        postItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    quantity = Integer.parseInt(editTextQuantity.getText().toString());
                }catch (NumberFormatException e){
                    e.printStackTrace();
                }

                if(!searched){
                    setLoading(false);
                    setWarning("Product Must be Searched", "message");
                }else if(quantity == 0){
                    setLoading(false);
                    setWarning("Invalid Quantity", "message");
                }else{
                    calculatorPresenter.quantityValidation(bearerToken
                            , editTextProductCode.getText().toString(), quantity);
                    setLoading(true);
                }
            }
        });
        return builder.create();
    }

    @Override
    public void updateItemDialog(String productName, int productPrice, int productId){
        setLoading(false);
        editTextProductName.setText(productName);
        editTextProductPrice.setText(kursIndonesia
                .format(productPrice));
        this.productPrice = productPrice;
        this.productId = productId;
        searched = true;
        warningText.setText("");
    }

    @Override
    public void postEntry(){
        listener.addItem(editTextProductName.getText().toString(),
                quantity, productPrice, productId);
        productPrice = productPrice * quantity;
        listener.calculatePrice(productPrice);
        dismiss();
    }

    @Override
    public void setWarning(String message, String type){
        if(type.equals("message")){
            warningText.setText(message);
            warningText.startAnimation(AnimationUtils.loadAnimation(getActivity()
                    , R.anim.shake_animation));
        }else if(type.equals("product")){
            warningText.setText(R.string.product_not_found);
            warningText.startAnimation(AnimationUtils.loadAnimation(getActivity()
                    , R.anim.shake_animation));
            editTextProductName.setText("");
            editTextProductPrice.setText("");
            productPrice = 0;
            productId = 0;
            quantity = 0;
            searched = false;
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (DialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement DialogListener");
        }
    }

    @Override
    public void setLoading(boolean status){
        if(status){
            warningText.setText("");
            searchProduct.setEnabled(false);
            postItem.setEnabled(false);
            loading.setVisibility(View.VISIBLE);
        }else{
            searchProduct.setEnabled(true);
            postItem.setEnabled(true);
            loading.setVisibility(View.GONE);
        }
    }

    public interface DialogListener{
        void calculatePrice(int productPrice);
        void addItem(String name, int quantity, int price, int productId);
    }


}
