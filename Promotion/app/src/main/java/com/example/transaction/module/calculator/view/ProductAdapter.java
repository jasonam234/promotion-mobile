package com.example.transaction.module.calculator.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.transaction.R;
import com.example.transaction.model.calculator.product.request.ProductRecycler;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {
    private ArrayList<ProductRecycler> productList;
    private OnItemClickListener clickListener;
    DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
    DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

    public interface OnItemClickListener{
        void onDeleteClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        clickListener = listener;
    }

    public static class ProductViewHolder extends RecyclerView.ViewHolder{
        public TextView productNameRecycler;
        public TextView productQuantityRecycler;
        public TextView productPriceRecycler;
        public TextView totalPriceRecycler;
        public ImageView cancelItem;

        public ProductViewHolder(View itemView, OnItemClickListener listener) {
            super(itemView);
            productNameRecycler = itemView.findViewById(R.id.productNameRecycler);
            productQuantityRecycler = itemView.findViewById(R.id.quantityRecycler);
            productPriceRecycler = itemView.findViewById(R.id.productPriceRecycler);
            totalPriceRecycler = itemView.findViewById(R.id.totalPriceRecycler);
            cancelItem = itemView.findViewById(R.id.cancelItem);

            cancelItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });
        }
    }

    public ProductAdapter(ArrayList<ProductRecycler> productList){
        this.productList = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_recycler
                , parent, false);
        ProductViewHolder productViewHolder = new ProductViewHolder(v, clickListener);
        return productViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        kursIndonesia.setDecimalFormatSymbols(formatRp);

        ProductRecycler currentItem = productList.get(position);
        holder.productNameRecycler.setText(currentItem.getName());
        holder.productQuantityRecycler.setText(String.valueOf(currentItem.getQuantity()));
        holder.productPriceRecycler.setText(kursIndonesia.format(currentItem.getPrice()));
        holder.totalPriceRecycler.setText(kursIndonesia
                .format(currentItem.getQuantity() * currentItem.getPrice()));
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }
}
