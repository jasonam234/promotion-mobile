package com.example.transaction.module.calculator.view;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.transaction.R;
import com.example.transaction.model.calculator.product.request.ProductRecycler;
import com.example.transaction.module.calculator.CalculatorContract;
import com.example.transaction.module.calculator.presenter.CalculatorPresenter;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class InformationDialog extends AppCompatDialogFragment
        implements CalculatorContract.InformationDialog {
    DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
    DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
    CalculatorContract.Presenter calculatorPresenter;
    private InformationDialog.InformationListener listener;
    ArrayList<ProductRecycler> productData = new ArrayList<>();

    public InformationDialog(CalculatorContract.Presenter calculatorPresenter
            , ArrayList<ProductRecycler> productData){
        this.calculatorPresenter = calculatorPresenter;
        this.productData = productData;
    }

    @NonNull
    @SuppressLint("SetTextI18n")
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.infromation_dialog, null);

        calculatorPresenter = new CalculatorPresenter(getContext(), this);
        calculatorPresenter.initPresenterDialog();

        TextView itemNameTextView = (TextView) view.findViewById(R.id.itemNameTextView);
        TextView promotionTypeTextView = (TextView) view.findViewById(R.id.promotionTypeTextView);

        for(int i = 0; i < productData.size(); i++){
            if(productData.get(i).getName().contains("*")){
                itemNameTextView.setText(productData.get(i).getName()
                        .substring(0, productData.get(i).getName().length() - 1));
                promotionTypeTextView.setText("Discount");
            }else if(productData.get(i).getPrice() == 0){
                promotionTypeTextView.setText("Free Goods");
            }
        }

        builder.setView(view)
                .setTitle("Promotion Info")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (InformationListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement DialogListener");
        }
    }

    public interface InformationListener{

    }
}
