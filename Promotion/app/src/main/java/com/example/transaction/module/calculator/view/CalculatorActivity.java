package com.example.transaction.module.calculator.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.auth0.android.jwt.JWT;
import com.example.transaction.R;
import com.example.transaction.model.calculator.product.request.ProductRecycler;
import com.example.transaction.module.calculator.CalculatorContract;
import com.example.transaction.module.calculator.presenter.CalculatorPresenter;
import com.example.transaction.module.login.view.LoginActivity;
import com.example.transaction.module.splash_screen.MainActivity;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CalculatorActivity extends AppCompatActivity
        implements CalculatorContract.View, AddItemDialog.DialogListener, PayDialog.PayListener
        , InformationDialog.InformationListener {
    RecyclerView productList;
    ProductAdapter productListAdapter;
    RecyclerView.LayoutManager productListLayout;
    CalculatorContract.Presenter calculatorPresenter;
    DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
    DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
    ArrayList<ProductRecycler> productData = new ArrayList<>();
    TextView subTotalPrice;
    TextView discountPrice;
    TextView totalPrice;
    Button payButton;
    Button activatePromoButton;
    Button addItemButton;
    ImageButton informationButton;
    ProgressBar progressBarCalculator;
    Toolbar toolbar;

    int subTotal = 0;
    int total = 0;
    int promotion = 0;
    int promotionId = 0;
    String productName;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void onComposeAction(MenuItem mi) {
        onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        subTotalPrice = (TextView) findViewById(R.id.subTotalPrice);
        discountPrice = (TextView) findViewById(R.id.discountPrice);
        totalPrice = (TextView) findViewById(R.id.totalPrice);
        payButton = (Button) findViewById(R.id.payButton);
        activatePromoButton = (Button) findViewById(R.id.activatePromoButton);
        addItemButton = (Button) findViewById(R.id.addItemButton);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        informationButton = (ImageButton) findViewById(R.id.informationButton);
        progressBarCalculator = (ProgressBar) findViewById(R.id.progressBarCalculator);
        setSupportActionBar(toolbar);

        productList = findViewById(R.id.recyclerView);
        productListLayout = new LinearLayoutManager(this);
        productListAdapter = new ProductAdapter(productData);
        productList.setLayoutManager(productListLayout);
        productList.setAdapter(productListAdapter);

        progressBarCalculator.setVisibility(View.GONE);

        productListAdapter.setOnItemClickListener(new ProductAdapter.OnItemClickListener() {
            @Override
            public void onDeleteClick(int position) {
                int minusPrice;

                if(productData.get(position).getName().contains("*")){
                    promotion = 0;
                    discountPrice.setText(kursIndonesia.format(promotion));
                }
                minusPrice = -(productData.get(position).getPrice() *
                        productData.get(position).getQuantity());
                calculatePrice(minusPrice);
                productData.remove(position);
                productListAdapter.notifyItemRemoved(position);
            }
        });

        calculatorPresenter = new CalculatorPresenter(this, this);
        calculatorPresenter.initPresenter();
    }

    public void initView(){
        int subTotal = 0;
        int discount = 0;
        int total = 0;

        SharedPreferences preferences = getSharedPreferences("token", MODE_PRIVATE);
        String token = preferences.getString("token", "");
        String bearerToken = "Bearer " + token;

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        kursIndonesia.setDecimalFormatSymbols(formatRp);

        subTotalPrice.setText(kursIndonesia.format(subTotal));
        discountPrice.setText(kursIndonesia.format(discount));
        totalPrice.setText(kursIndonesia.format(total));

        progressBarStatus(false);

        addItemButton.setOnClickListener(v -> openDialog());
        payButton.setOnClickListener(v -> openPayDialog());
        informationButton.setOnClickListener(v -> openInfoDialog());
        activatePromoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBarStatus(true);
                calculatorPresenter.applyDiscount(bearerToken, productData);
            }
        });
    }

    public void openDialog(){
        AddItemDialog addItemDialog = new AddItemDialog(calculatorPresenter);
        addItemDialog.show(getSupportFragmentManager(), "Add New Item");
    }

    public void openPayDialog(){
        if(productData.size() == 0){
            sweetAlert("fail", "Item Must be Added !");
        }else{
            PayDialog payDialog = new PayDialog();
            payDialog.show(getSupportFragmentManager(), "Pay");
        }
    }

    public void openInfoDialog(){
        InformationDialog infoDialog = new InformationDialog(calculatorPresenter, productData);
        infoDialog.show(getSupportFragmentManager(), "Information");
    }

    public void calculatePrice(int productPrice){
        subTotal = productPrice + subTotal;
        total = subTotal - promotion;
        subTotalPrice.setText(kursIndonesia.format(subTotal));
        totalPrice.setText(kursIndonesia.format(total));

        if(total < 0){
            promotion = 0;
            discountPrice.setText(kursIndonesia.format(0));
            totalPrice.setText(kursIndonesia.format(subTotal));
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    public void addItem(String name, int quantity, int productPrice, int productId){
        productData.add(new ProductRecycler(name, quantity, productPrice, productId));
        productListAdapter.notifyDataSetChanged();
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void postTransaction(String description) {
        SharedPreferences preferences = getSharedPreferences("token", MODE_PRIVATE);
        String token = preferences.getString("token", "");
        String bearerToken = "Bearer " + token;

        JWT jwt = new JWT(token);
        int userId = jwt.getClaim("user_id").asInt();

        progressBarStatus(true);

        calculatorPresenter.postTransaction(userId, description, total, productData, promotionId
                , bearerToken);

        subTotal = 0;
        promotion = 0;
        total = 0;
        subTotalPrice.setText(kursIndonesia.format(subTotal));
        totalPrice.setText(kursIndonesia.format(total));
        discountPrice.setText(kursIndonesia.format(promotion));
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void clearData(){
        productData.clear();
        productListAdapter.notifyDataSetChanged();
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void applyDiscount(int discountValue, int price, int productId, int promotionId
            , String itemName){
        promotion = (price * discountValue)/100;

        for(int i = 0; i < productData.size(); i++){
            productName = productData.get(i).getName();
            if(productName.contains("*")){
                productData.get(i).setName(productName
                        .substring(0, productData.get(i).getName().length() - 1));
            }
        }

        for(int i = 0; i < productData.size(); i++){
            productName = productData.get(i).getName();
            Log.i("PRODUCT NAME", productName);
            if(productData.get(i).getProductId() == productId){
                productData.get(i).setName(productName.concat("*"));
                promotion = promotion * productData.get(i).getQuantity();
                productListAdapter.notifyDataSetChanged();
            }
        }

        productListAdapter.notifyDataSetChanged();

        total = subTotal - promotion;
        this.promotionId = promotionId;
        discountPrice.setText(kursIndonesia.format(promotion));
        totalPrice.setText(kursIndonesia.format(total));
        progressBarStatus(false);
        sweetAlert("success", "Promo Discount Applied");
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void applyFreeGoods(String name, int quantity, int productId, int promotionId){
        this.promotionId = promotionId;

        for(int i = 0; i < productData.size(); i++){
            productName = productData.get(i).getName();
            if(productName.contains("*")){
                productData.get(i).setName(productName
                        .substring(0, productData.get(i).getName().length() - 1));
            }
            if(productData.get(i).getPrice() == 0){
                productData.remove(i);
            }
        }

        if(productData.get(productData.size()-1).getPrice() != 0){
            productData.add(new ProductRecycler(name, quantity, 0, productId));
        }

        for(int i = 0; i < productData.size(); i++){
            if(productData.get(i).getPrice() == 0){
                productData.get(i-1)
                        .setName(productData.get(i-1).getName().concat("*"));
            }
        }
        productListAdapter.notifyDataSetChanged();
        promotion = 0;
        total = subTotal;
        discountPrice.setText(kursIndonesia.format(promotion));
        totalPrice.setText(kursIndonesia.format(total));
        progressBarStatus(false);
        sweetAlert("success", "Promo Free Goods Applied");
    }

    @Override
    public void sweetAlert(String type, String title){
        switch (type) {
            case "success":
                new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText(title)
                        .show();
                break;
            case "fail":
                new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(title)
                        .show();
                break;
        }
    }

    @Override
    public void progressBarStatus(boolean status){
        if(status){
            informationButton.setEnabled(false);
            activatePromoButton.setEnabled(false);
            addItemButton.setEnabled(false);
            payButton.setEnabled(false);
            progressBarCalculator.setVisibility(View.VISIBLE);
        }else{
            informationButton.setEnabled(true);
            activatePromoButton.setEnabled(true);
            addItemButton.setEnabled(true);
            payButton.setEnabled(true);
            progressBarCalculator.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStop () {
        super.onStop();
        finish();
        SharedPreferences mToken = this
                .getSharedPreferences("token", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefEdit = mToken.edit();
        prefEdit.clear();
        prefEdit.apply();
        startActivity(new Intent(CalculatorActivity.this, LoginActivity.class));
    }
}